'use strict';

const { parseExpression } = require('./expression');
const { expect } = require('chai');

describe('parseExpression', () => {
    it('is exported', () => {
        expect(parseExpression).to.be.a('function');
    });
    it('parses to a function', () => {
        expect(parseExpression('1')).to.be.a('function');
    })
    it('resolves constants', () => {
        const expr = parseExpression('154');
        expect(expr(1)).to.equal('154');
    });
    it('resolves input value', () => {
        const expr = parseExpression('value');
        expect(expr(110)).to.equal(110);
    });

    it('supports nested expressions', () => {
        const expr = parseExpression({
            left: {
                left: {
                    left: 'value',
                    right: '100',
                    op: '==',
                },
                right: {
                    left: '100',
                    right: '100',
                    op: '==',
                },
                op: '&&',
            },
            right: {
                left: '100',
                right: '101',
                op: '==',
            },
            op: '||'
        });
        expect(expr('100')).to.be.true;
        expect(expr('101')).to.be.false;
    })

    describe('== operator', () => {
        const expr = parseExpression({
            left: '100',
            right: 'value',
            op: '==',
        });
        it('parses', () => {
            expect(expr).to.be.a('function');
        });
        it('determines two equal', () => {
            expect(expr(100)).to.be.true;
        });
        it('determines two not equal', () => {
            expect(expr(101)).to.be.false;
        });
        it('determines two equal with value of lhs', () => {
            const exprLhs = parseExpression({
                left: 'value',
                right: '100',
                op: '==',
            });
            expect(exprLhs(100)).to.be.true;
        });
    });
    describe('!= operator', () => {
        const expr = parseExpression({
            left: '100',
            right: 'value',
            op: '!=',
        });
        it('parses', () => {
            expect(expr).to.be.a('function');
        });
        it('determines two non equal', () => {
            expect(expr(101)).to.be.true;
        });
        it('determines two equal', () => {
            expect(expr(100)).to.be.false;
        });
    });
    describe('< operator', () => {
        const expr = parseExpression({
            left: '100',
            right: 'value',
            op: '<',
        });
        it('parses', () => {
            expect(expr).to.be.a('function');
        });
        it('gives true for a greater value', () => {
            expect(expr(101)).to.be.true;
        });
        it('gives false for a smaller value', () => {
            expect(expr(99)).to.be.false;
        });
        it('gives false for an equal value', () => {
            expect(expr(100)).to.be.false;
        });
    });

    describe('<= operator', () => {
        const expr = parseExpression({
            left: '100',
            right: 'value',
            op: '<=',
        });
        it('parses', () => {
            expect(expr).to.be.a('function');
        });
        it('gives true for a larger value', () => {
            expect(expr(101)).to.be.true;
        });
        it('gives false for a smaller value', () => {
            expect(expr(99)).to.be.false;
        });
        it('gives true for an equal value', () => {
            expect(expr(100)).to.be.true;
        });
    });

    describe('> operator', () => {
        const expr = parseExpression({
            left: '100',
            right: 'value',
            op: '>',
        });
        it('parses', () => {
            expect(expr).to.be.a('function');
        });
        it('gives true for a smaller value', () => {
            expect(expr(99)).to.be.true;
        });
        it('gives false for a larger value', () => {
            expect(expr(101)).to.be.false;
        });
        it('gives false for an equal value', () => {
            expect(expr(100)).to.be.false;
        });
    });

    describe('>= operator', () => {
        const expr = parseExpression({
            left: '100',
            right: 'value',
            op: '>=',
        });
        it('parses', () => {
            expect(expr).to.be.a('function');
        });
        it('gives true for a smaller value', () => {
            expect(expr(99)).to.be.true;
        });
        it('gives false for a larger value', () => {
            expect(expr(101)).to.be.false;
        });
        it('gives true for an equal value', () => {
            expect(expr(100)).to.be.true;
        });
    });

    describe('logical operators', () => {
        const trueExpr = {
            left: '100',
            right: '100',
            op: '=='
        };
        const falseExpr = {
            left: '100',
            right: '101',
            op: '==',
        };

        describe('&& operator', () => {
            it('parses', () => {
                const expr = parseExpression({
                    left: trueExpr,
                    right: trueExpr,
                    op: '&&',
                });
                expect(expr).to.be.a('function');
            });
            it('gives true for two true expressions', () => {
                const expr = parseExpression({
                    left: trueExpr,
                    right: trueExpr,
                    op: '&&',
                });
                expect(expr('1')).to.be.true;
            });
            it('gives false for one true and one false', () => {
                const expr = parseExpression({
                    left: trueExpr,
                    right: falseExpr,
                    op: '&&',
                });
                expect(expr('1')).to.be.false;
            });
            it('gives false for both false', () => {
                const expr = parseExpression({
                    left: falseExpr,
                    right: falseExpr,
                    op: '&&',
                });
                expect(expr('1')).to.be.false;
            });
        });
        describe('|| operator', () => {
            it('parses', () => {
                const expr = parseExpression({
                    left: trueExpr,
                    right: trueExpr,
                    op: '||',
                });
                expect(expr).to.be.a('function');
            });
            it('gives true for two true expressions', () => {
                const expr = parseExpression({
                    left: trueExpr,
                    right: trueExpr,
                    op: '||',
                });
                expect(expr('1')).to.be.true;
            });
            it('gives true for one true and one false', () => {
                const expr = parseExpression({
                    left: trueExpr,
                    right: falseExpr,
                    op: '||',
                });
                expect(expr('1')).to.be.true;
            });
            it('gives false for both false', () => {
                const expr = parseExpression({
                    left: falseExpr,
                    right: falseExpr,
                    op: '||',
                });
                expect(expr('1')).to.be.false;
            });
        });
    })
});