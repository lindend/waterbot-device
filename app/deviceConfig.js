const store = require('./configStore');
const { deviceId, uploadDeviceInitKey } = require('./device');
const { db } = require('./firebase');
const { error } = require('./log');
const { firebaseSubscribeErrorBackOff } = require('./constants');

const defaultDeviceConfig = {
    wateringCheckInterval: 12,
    userId: null,
    
    channels: {
        // default: {
        //     sensor: 'moisture',
        //     agent: 'pump',
        //     duration: 60,
        // }
    },

    sensors: {
        // moisture: {
        //     type: 'moisture',
        //     pin: 11,
        //     activationPin: 12,
        // },
        // temperature: {
        //     type: 'dht11',
        //     pin: 13,
        // }
    },

    agents: {
        // pump: {
        //     type: 'digital_pin',
        //     pin: 7
        // }
    }
};

let config = {
    current: defaultDeviceConfig,
}

function updateCurrentDeviceConfig(newConfig) {
    config.current = newConfig;
    if (newConfig.userId == null) {
        uploadDeviceInitKey();
    }

    store.put('config', newConfig);
}

const dbDocumentRef = db.collection('devices').doc(deviceId);

function subscribeToSnapshots(resolve) {
    dbDocumentRef.onSnapshot(snapshot => {
        if (snapshot.exists) {
            updateCurrentDeviceConfig(snapshot.data());
        } else {
            uploadDeviceInitKey();
        }
        resolve();
    },
    err => {
        error('Unable to subscribe to config updates (retrying): ' + err.message);

        setTimeout(subscribeToSnapshots, firebaseSubscribeErrorBackOff);
        resolve();
    });
}

function initialize() {
    const currentDeviceConfig = store.get('config');
    if (currentDeviceConfig != null) {
        config.current = currentDeviceConfig;
    }

    return new Promise(function(resolve, reject) {
        subscribeToSnapshots(resolve);
    });
}

module.exports.config = config;
module.exports.initializeConfig = initialize;