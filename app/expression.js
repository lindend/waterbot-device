function parseExpression(expression) {
    if (typeof expression === 'string') {
        if (expression === 'value') {
            return (v) => v;
        } else {
            return (v) => expression;
        }
    }

    const left = parseExpression(expression.left);
    const right = parseExpression(expression.right);
    switch (expression.op) {
        case '&&':
            return (v) => left(v) && right(v);
        case '||':
            return (v) => left(v) || right(v);
        case '==':
            return (v) => left(v) == right(v);
        case '!=':
            return (v) => left(v) != right(v);
        case '<':
            return (v) => left(v) < right(v);
        case '<=':
            return (v) => left(v) <= right(v);
        case '>':
            return (v) => left(v) > right(v);
        case '>=':
            return (v) => left(v) >= right(v);
    }
}

module.exports.parseExpression = parseExpression;