let rpio = require('rpio');

function activateAgent(agent, duration) {
    rpio.open(agent.pin, rpio.OUTPUT, rpio.LOW);
    rpio.write(agent.pin, 1);
    rpio.sleep(duration);
    rpio.write(agent.pin, 0);
}

module.exports.activateAgent = activateAgent;