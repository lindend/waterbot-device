let rpio = require('rpio');
let rpiDhtSensor = require('rpi-dht-sensor');

const numSensorReadings = 5;
const numSensorReadingsForHigh = 3;
const sensorReadingInterval = 100;

function readGenericSensor(sensor) {
    rpio.open(sensor.pin, rpio.INPUT);

    if (sensor.activationPin != null) {
        rpio.open(sensor.activationPin, rpio.OUTPUT, rpio.LOW);
        rpio.write(sensor.activationPin, 1);
        rpio.msleep(200);
    }

    let results = [];
    for (let i = 0; i < numSensorReadings; ++i) {
        results.push(rpio.read(sensor.pin));
        rpio.msleep(sensorReadingInterval);
    }

    if (sensor.activationPin != null) {
        rpio.write(sensor.activationPin, 0);
    }

    let sum = results.reduce((a, b) => a + b, 0);
    if (sum >= numSensorReadingsForHigh) {
        return 1;
    } else {
        return 0;
    }
}

function readAnalogSensor(sensor) {
    if (sensor.activationPin != null) {
        rpio.open(sensor.activationPin, rpio.OUTPUT, rpio.LOW);
        rpio.write(sensor.activationPin, 1);
        rpio.msleep(200);
    }
    const channel = sensor.channel;

    const tx = new Buffer([0x01, 8 + channel << 4, 0x01]);
    let rx = new Buffer(tx.length);
    
    rpio.spiTransfer(tx, rx, tx.length);

    if (sensor.activationPin != null) {
        rpio.write(sensor.activationPin, 0);
    }

    const msb = rx[1],
          lsb = rx[2];
    
    const result = ((msb & 3) << 8) + lsb;

    return result;
}

function readTemperatureSensor(sensor) {
    let dht = new rpiDhtSensor.DHT11(sensor.pin);
    let result = {
        t: 0,
        h: 0,
    };
    let numSuccessfulReadings = 0;

    for (let i = 0; i < numSensorReadings; ++i) {
        let reading = dht.read();
        if (reading.error == null && reading.isValid) {
            result.t += reading.temperature;
            result.h += reading.humidity;
            numSuccessfulReadings += 1;
        }
        rpio.msleep(sensorReadingInterval);
    }
    if (numSuccessfulReadings > 0) {
        result.t /= numSuccessfulReadings;
        result.h /= numSuccessfulReadings;
        return result;
    } else {
        return null;
    }
}

const sensorTypes = {
    moisture: readGenericSensor,
    moisture_analog: readAnalogSensor,
    dht11: readTemperatureSensor,
};

function readSensor(sensor) {
    let reader = sensorTypes[sensor.type];
    if (reader != null) {
        return reader(sensor);
    }
    return null;
}

module.exports.readSensor = readSensor;