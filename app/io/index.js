const { activateAgent } = require('./agent');
const { readSensor } = require('./sensor');
const { initializeIo } = require('./init');

module.exports.activateAgent = activateAgent;
module.exports.readSensor = readSensor;
module.exports.initializeIo = initializeIo;