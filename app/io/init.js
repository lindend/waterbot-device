const rpio = require('rpio');

function initializeIo() {
    rpio.spiBegin();
    rpio.spiChipSelect(0);
    rpio.spiSetCSPolarity(0, rpio.LOW);
    rpio.spiSetClockDivider(256);
    rpio.spiSetDataMode(0)
}

module.exports.initializeIo = initializeIo;