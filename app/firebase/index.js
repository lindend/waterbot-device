let firebase = require('firebase');

var config = {
    apiKey: "AIzaSyAx3uxQ3d_lxfh-46axWNqUnfDbbYpUDHk",
    authDomain: "waterbot-1.firebaseapp.com",
    databaseURL: "https://waterbot-1.firebaseio.com",
    projectId: "waterbot-1",
    storageBucket: "waterbot-1.appspot.com",
    messagingSenderId: "666967857770"
};
firebase.initializeApp(config);

const db = firebase.firestore();
const settings = { timestampsInSnapshots: true };
db.settings(settings);

const serverTimestamp = firebase.firestore.FieldValue.serverTimestamp;

module.exports.db = db;
module.exports.serverTimestamp = serverTimestamp;
