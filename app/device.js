const crypto = require('crypto');
const store = require('./configStore');
const { initKeySize, deviceIdSize } = require('./constants');
const { db } = require('./firebase');
const { initKeyValidTime } = require('./constants');
const { error } = require('./log');
const os = require('os');

const deviceId = store.get('deviceId') || generateKey(deviceIdSize);
store.put('deviceId', deviceId);

console.log('***********************************************************************************');
console.log('    DEVICE ID: ' + deviceId);
console.log('***********************************************************************************');
console.log('');

const initKey = generateKey(initKeySize);

console.log('*********************************');
console.log('    DEVICE INIT KEY: ' + initKey);
console.log('*********************************');

function generateKey(size) {
    const bytes = crypto.randomBytes(size);
    return Buffer.from(bytes).toString('hex');
}

let initKeyUploaded = false;

function uploadDeviceInitKey() {
    if (initKeyUploaded) {
        return;
    }

    let validUntil = new Date(new Date().toUTCString().substr(0, 25));
    validUntil.setTime(validUntil.getTime() + initKeyValidTime)
    const initKeyDoc = {
        deviceId: deviceId,
        validUntil: validUntil,
    };
    db.collection('init-tokens').doc(initKey).set(initKeyDoc)
        .then(res => {
            initKeyUploaded = true;
        })
        .catch(err => {
            error(`Unable to upload init key to db: ${err.message}`);
        });
}

function getIpAddresses() {
    const networkInterfaces = os.networkInterfaces();
    let addresses = [];
    for (let interfaceKey of Object.keys(networkInterfaces)) {
        const interfaceAddresses = networkInterfaces[interfaceKey];
        for (let address of interfaceAddresses) {
            if (!address.internal) {
                addresses.push(address.address);
            }
        }
    }
    return addresses;
}

function uploadDeviceInfo() {
    db.collection('devices')
        .doc(deviceId)
        .collection('info')
        .doc('info')
        .set({
            addresses: getIpAddresses(),
        })
        .catch(() => {
            setTimeout(() => uploadDeviceInfo(), 30000);
        })
}

uploadDeviceInfo();

module.exports.store = store;
module.exports.deviceId = deviceId;
module.exports.initKey = initKey;
module.exports.uploadDeviceInitKey = uploadDeviceInitKey;
