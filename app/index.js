const { initializeConfig } = require('./deviceConfig');
const { initializeActivation } = require('./agentActivation');
const { initializeStats } = require('./statsUpload');
const { initializeIo } = require('./io');

initializeIo();
initializeConfig().then(() => {
    initializeActivation();
    initializeStats();
});