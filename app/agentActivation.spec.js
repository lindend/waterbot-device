'use strict';

const proxyquire = require('proxyquire');
const { expect } = require('chai');
const sinon = require('sinon');

let checkWatering;
let ioStub,
    configStub,
    statsUploadStub;

function buildConfiguration(...channels) {
    let agents = {};
    let configChannels = {};
    let sensors = {};
    for (let channel of channels) {
        agents[channel.name] = {
            name: channel.name,
        };
        if (channel.sensor) {
            const sensor = {
                name: channel.name,
            };
            sensors[channel.name] = sensor;
            ioStub.readSensor.withArgs(sensor).returns(channel.sensor.reading);
        }
        configChannels[channel.name] = {
            agent: channel.name,
            duration: channel.duration,
            sensor: channel.sensor ? channel.name : undefined,
            disabled: channel.disabled,
            expression: channel.expression,
        };
    }

    configStub.config.current = {
        agents: agents,
        sensors: sensors,
        channels: configChannels,
    };

    return {agents, sensors, configChannels};
}

describe('agentActivation', () => {
    beforeEach(() => {
        ioStub = { activateAgent: sinon.stub(), readSensor: sinon.stub() };
        configStub = { config: { }};
        statsUploadStub = { addStat: sinon.fake() };

        checkWatering = proxyquire('./agentActivation', {
            './deviceConfig': configStub,
            './io': ioStub,
            './statsUpload': statsUploadStub,
        }).checkWatering;
    });

    it('activates channels with no sensor', () => {
        buildConfiguration({ name: 'ch0', duration: 15});
        
        checkWatering(false);

        expect(ioStub.activateAgent.called).to.be.ok;
    });

    it('passes the agent and duration to activations', () => {
        const {agents} = buildConfiguration({ name: 'ch0', duration: 15});

        checkWatering(false);

        expect(ioStub.activateAgent.calledWith(agents['ch0'], 15)).to.be.ok;
    });

    it('activates channels with a sensor that returns 1 (no expression)', () => {
        const {agents} = buildConfiguration({name: 'ch0', duration: 12, sensor: {reading: 1}});
        checkWatering(false);

        expect(ioStub.activateAgent.calledWith(agents['ch0'], 12)).to.be.ok;
    });

    it('does not activate channels with a sensor that returns 0 (no expression)', () => {
        buildConfiguration({name: 'ch0', duration: 14, sensor: { reading: 0}});

        checkWatering(false);

        expect(ioStub.activateAgent.called).to.be.false;
    })

    it('activates channels with force set to true', () => {
        const {agents} = buildConfiguration({name: 'ch0', duration: 14, sensor: { reading: 0}});

        checkWatering(true);

        expect(ioStub.activateAgent.calledWith(agents['ch0'], 14)).to.be.ok;
    });

    it('does not activate disabled channels', () => {
        buildConfiguration({name: 'ch0', disabled: true});

        checkWatering(false);

        expect(ioStub.activateAgent.called).to.be.false;
    });

    it('defaults to 10s activation time', () => {
        const {agents} = buildConfiguration({ name: 'ch0' });

        checkWatering(false);

        expect(ioStub.activateAgent.calledWith(agents['ch0'], 10)).to.be.ok;
    });

    it('uses expressions to check sensor readings', () => {
        const {agents} = buildConfiguration({ 
            name: 'ch0', 
            sensor: { reading: 105 }, 
            expression: {
                left: 'value',
                right: '105',
                op: '==',
            }
        });

        checkWatering(false);

        expect(ioStub.activateAgent.calledWith(agents['ch0'], 10)).to.be.ok;
    });

    it('does not activate for expressions evaluating to false', () => {
        const {agents} = buildConfiguration({ 
            name: 'ch0', 
            sensor: { reading: 104 }, 
            expression: {
                left: 'value',
                right: '105',
                op: '==',
            }
        });

        checkWatering(false);

        expect(ioStub.activateAgent.called).to.be.false;
    });
});
