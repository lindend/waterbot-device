function log(severity, message) {
    console.log(`${new Date()} | ${severity} |${message}`);
}

module.exports.log = function(message) {
    log('Info', message);
}

module.exports.error = function(message) {
    log('ERROR', message);
}