const { config } = require('./deviceConfig');
const { activateAgent, readSensor } = require('./io');
const { log } = require('./log');
const { addStat } = require('./statsUpload');
const { parseExpression } = require('./expression');

let hoursSinceLastCheck = 0;

function checkTimer() {
    hoursSinceLastCheck += 1;
    log(`${hoursSinceLastCheck} hours since last watering check`)
    if (hoursSinceLastCheck >= config.current.wateringCheckInterval) {
        checkWatering();
    }
}

function checkWatering(force) {
    log('Checking channels for watering');
    for (let channelId of Object.keys(config.current.channels)) {
        const channel = config.current.channels[channelId];
        if (channel.disabled) {
            log(`Skipping disabled channel '${channelId}'`)
            continue;
        }

        const sensor = config.current.sensors[channel.sensor];
        if (sensor == null && channel.sensor != null) {
            error(`Invalid sensor name for channel ${channelId}: '${channel.sensor} does not exist`);
            continue;
        }

        const sensorValue = sensor && readSensor(sensor);
        log(`Got value ${sensorValue} from sensor ${channel.sensor} on channel ${channelId}`);
        
        const evaluator = getSensorValueEvaluator(channel);

        if (force || channel.sensor == null || evaluator(sensorValue)) {
            log(`Watering channel '${channelId}'`);
            registerActivation(channelId);
            const agent = config.current.agents[channel.agent];
            const duration = channel.duration || 10;
            activateAgent(agent, duration);
        }
    }

    hoursSinceLastCheck = 0;    
}

function getSensorValueEvaluator(channel) {
    if (channel.expression != null) {
        return parseExpression(channel.expression);
    } else {
        return v => v == 1;
    }
}

function registerActivation(channelId) {
    addStat(channelId, 'channel', 'event');
}

function initialize() {
    checkWatering();

    setInterval(checkTimer, 3600 * 1000);
}

module.exports.initializeActivation = initialize;
module.exports.checkWatering = checkWatering;