const { deviceId } = require('./device');
const { db } = require('./firebase');
const { config } = require('./deviceConfig');
const configStore = require('./configStore');
const { readSensor } = require('./io');
const { log, error } = require('./log');
const { statsFrequency } = require('./constants');

const deviceDoc = db.collection('devices').doc(deviceId);

let currentStatsDocument = {
    month: -1,
    key: '',
    data: null,
};

function getCurrentStatsDocument() {
    return new Promise((resolve, reject) => {
        const now = new Date();
        if (currentStatsDocument.month == now.getMonth()) {
            resolve(currentStatsDocument);
        }

        currentStatsDocument.key = `${now.getFullYear()}-${now.getMonth() + 1}`;

        deviceDoc.collection('stats')
            .doc(currentStatsDocument.key)
            .get()
            .then(result => {
                currentStatsDocument.month = now.getMonth();
                if (result.exists) {
                    currentStatsDocument.data = result.data();
                } else {
                    currentStatsDocument.data = {
                    };
                }
                resolve(currentStatsDocument);
            })
            .catch(error => {
                reject(error);
            });
    });
}

function gatherStats(statsDocument) {
    log('Collecting sensor values');

    for (let sensorId of Object.keys(config.current.sensors)) {
        let sensor = config.current.sensors[sensorId];
        let sensorValue = readSensor(sensor);
        if (sensorValue == null) {
            continue;
        }

        addStatToDoc(sensorId, 'sensor', sensor.type, sensorValue, statsDocument);
    }
}

function addStat(sourceId, category, type, value) {
    return addStats([{
        source: sourceId,
        category: category,
        type: type,
        value: value,
    }]);
}

function addStats(stats) {
    return getCurrentStatsDocument()
    .then(doc => {
        for (let stat of stats) {
            addStatToDoc(stat.source, stat.category, stat.type, stat.value, doc.data);
        }
        uploadStats(doc);
    });
}

function addStatToDoc(sourceId, category, type, value, statsDoc) {
    const sourceKey = `${category}-${sourceId}`;
    let sourceDoc = statsDoc[sourceKey];
    if (sourceDoc === undefined) {
        sourceDoc = {
            id: sourceId,
            category: category,
            type: type,
            values: []
        };
        statsDoc[sourceKey] = sourceDoc;
    }
    let dataPoint = {
        t: new Date()
    }
    if (value != null) {
        dataPoint.v = value;
    }
    sourceDoc.values.push(dataPoint);   
}

function uploadStats(currentStats) {
    return deviceDoc.collection('stats')
        .doc(currentStats.key)
        .set(currentStats.data)
        .then(() => {
            configStore.put('currentStats', currentStatsDocument);
        })
        .catch(err => {
            error('Unable to upload sensor data: ' + err.message);
        });
}

function gatherAndUploadStats() {
    return getCurrentStatsDocument()
        .then(doc => {
            gatherStats(doc.data);
            uploadStats(doc);
        });
}

function initialize() {
    gatherAndUploadStats();

    setInterval(gatherAndUploadStats, statsFrequency);
}

module.exports.initializeStats = initialize;
module.exports.addStat = addStat;
module.exports.addStats = addStats;