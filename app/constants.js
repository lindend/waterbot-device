module.exports.initKeyValidTime = 12 * 3600 * 1000; // 12 hrs
module.exports.uploadInitKeyBackOff = 5 * 60 * 1000; // 5 mins

module.exports.statsFrequency = 3600 * 1000; // 1 hr
module.exports.maxStatsEntries = 5000; 

module.exports.firebaseSubscribeErrorBackOff = 6 * 3600 * 1000; // 6 hrs

module.exports.initKeySize = 4;
module.exports.deviceIdSize = 32;