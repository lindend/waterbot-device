#!/bin/sh
set -e

do_install() {
    if [ $(id -u) -ne 0 ]; then
        echo "Please run as root"
        exit
    fi
    apt-get update
    apt-get install -y git
    rm -rf /var/waterbot
    git clone https://gitlab.com/lindend/waterbot-device.git /var/waterbot
    /var/waterbot/install/install.sh
}

do_install