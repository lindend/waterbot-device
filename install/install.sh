#!/bin/sh

if [ $(id -u) -ne 0 ]; then
  echo "Please run as root"
  exit
fi

/var/waterbot/install/install-node.sh

# Set up user and permissions
WATERBOT_USER=root

id -u $WATERBOT_USER >/dev/null 2>&1 || useradd $WATERBOT_USER
mkdir /home/waterbot
chown -R $WATERBOT_USER:$WATERBOT_USER /home/waterbot

chown -R $WATERBOT_USER:$WATERBOT_USER /var/waterbot

# Give acess to /dev/gpiomem and /dev/mem
adduser waterbot gpio 2>/dev/null >/dev/null || true
adduser waterbot kmem 2>/dev/null >/dev/null || true

# Install systemd service
cp /var/waterbot/install/waterbot.service /etc/systemd/system
systemctl daemon-reload
systemctl enable waterbot
systemctl start waterbot

echo "Waterbot successfully installed, view the logs with"
echo "      journalctl -u waterbot"

